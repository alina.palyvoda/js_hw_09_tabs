/**
 * Created by Алина on 15.01.2022.
 */

let tabs = document.querySelector(".tabs");
let tabContent = document.getElementsByClassName("content");
let tab = document.getElementsByClassName("tab-title");
hideTabsContent(1);

tabs.addEventListener("click", function (event) {
    let target = event.target;
    if (target.className === "tab-title") {
        for (let i = 0; i < tab.length; i++) {
            if (target === tab[i]) {
                showTabsContent(i);
                break;
            }
        }
    }
})

function hideTabsContent(a) {
    for (let i = a; i < tabContent.length; i++) {
        tabContent[i].classList.remove('show');
        tabContent[i].classList.add("hide");
        tab[i].classList.remove('active');
    }
}

function showTabsContent(b) {
    if (tabContent[b].classList.contains('hide')) {
        hideTabsContent(0);
        tab[b].classList.add('active');
        tabContent[b].classList.remove('hide');
        tabContent[b].classList.add('show');
    }
}

